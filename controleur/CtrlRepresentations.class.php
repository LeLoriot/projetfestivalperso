<?php

namespace controleur;

use controleur\GestionErreurs;
use modele\dao\RepresentationDAO;
use modele\metier\Groupe;
use modele\dao\GroupeDAO;
use modele\metier\Lieu;
use modele\dao\LieuDAO;
use modele\metier\Representation;
use modele\dao\Bdd;
use vue\representations\vueListeRepresentations;
use vue\representations\VueSaisieRepresentation;
use vue\representations\VueSupprimerRepresentations;

ini_set('display_errors', 'on');

class CtrlRepresentations extends ControleurGenerique {
    public function defaut() {
        $this->liste();
    }
    public function liste() {
        $laVue = new VueListeRepresentations();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des representations
        Bdd::connecter();
        $laVue->setLesRepresentations(RepresentationDAO::getAll());
        
        
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representations");
        $this->vue->afficher();
    }  


/** controleur= representation & action=creer
     * Afficher le formulaire d'ajout d'une représenatation     */
    public function creer() {
        Bdd::connecter();
        $laVue = new VueSaisieRepresentation();
        $this->vue = $laVue;
        $this->vue->setListeGroupe(GroupeDAO::getAll());
        $this->vue->setListeLieu(LieuDAO::getAll());
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouvelle représentation");
        // En création, on affiche un formulaire vide
        /* @var Representation $uneRepresentation */
        $unGroupe = new Groupe ("","","","","","","");
        $unLieu = new Lieu("","","","");
        $uneRepresentation = new Representation("",$unGroupe, $unLieu, "", "", "");
        $laVue->setUneRepresentation($uneRepresentation);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representations");
        $this->vue->afficher();
    }
    
    /** controleur= representations & action=validerCreer
     * ajouter d'une représentation dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        /* @var Representation $uneRep  : récupération du contenu du formulaire et instanciation d'une représentation */
        $uneRep = new Representation($_REQUEST['id'], GroupeDAO::getOneById($_REQUEST['groupe']), LieuDAO::getOneById($_REQUEST['lieu']), $_REQUEST['date'], $_REQUEST['heureDebut'], $_REQUEST['heureFin']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesRep($uneRep, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer la représentation
            RepresentationDAO::insert($uneRep);
            // revenir à la liste des représentations
            header("Location: index.php?controleur=representations&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieRepresentation();
            $this->vue = $laVue;
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Nouvelle représentation");
            $laVue->setUneRepresentation($uneRep);
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - représentations");
            $this->vue->afficher();
        }
    }
    
    
/** controleur= representations & action=modifier $ id=identifiant de la représentation à modifier
     * Afficher le formulaire de modification d'une représentation     */
    public function modifier() {
        Bdd::connecter();
        $idRep = $_GET["id"];
        $laVue = new VueSaisieRepresentation();
        $this->vue = $laVue;
        $this->vue->setListeGroupe(GroupeDAO::getAll());
        $this->vue->setListeLieu(LieuDAO::getAll());
        // Lire dans la BDD les données de la représentation à modifier
        
        /* @var Representation $laRepresentation */
        $laRepresentation = RepresentationDAO::getOneById($idRep);
        $this->vue->setUneRepresentation($laRepresentation);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier la représentation :  (" . $laRepresentation->getId() . ")");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representations");
        $this->vue->afficher();
    }

    /** controleur= representations & action=validerModifier
     * modifier une représentation dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        /* @var Representation $uneRep  : récupération du contenu du formulaire et instanciation d'une représentation */
        $uneRep = new Representation($_REQUEST['id'], GroupeDAO::getOneById($_REQUEST['groupe']), LieuDAO::getOneById($_REQUEST['lieu']), $_REQUEST['date'], $_REQUEST['heureDebut'], $_REQUEST['heureFin']);

        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesRep($uneRep, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour l'établissement
            RepresentationDAO::update($uneRep->getId(), $uneRep);
            // revenir à la liste des représentations
            header("Location: index.php?controleur=representations&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieRepresentation();
            $this->vue = $laVue;
            $laVue->setUneRepresentation($uneRep);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier l'établissement : " . $uneRep->getNom() . " (" . $uneRep->getId() . ")");
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - etablissements");
            $this->vue->afficher();
        }
    }
    
    
/** controleur= representations & action=supprimer & id=identifiant_representation
     * Supprimer une représentation d'après son identifiant     */
    public function supprimer() {
        $idRep = $_GET["id"];
        $this->vue = new VueSupprimerRepresentations();
        // Lire dans la BDD les données de la représentation à supprimer
        Bdd::connecter();
        $this->vue->setUneRepresentation(RepresentationDAO::getOneById($idRep));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->afficher();
    }

    /** controleur= representations & action= validerSupprimer
     * supprimer une représentation dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant de la représentation à supprimer");
        } else {
            // suppression de la représentation d'après son identifiant
            RepresentationDAO::delete($_GET["id"]);
        }
        // retour à la liste des représentations
        header("Location: index.php?controleur=representations&action=liste");
    }
    
 /**
     * Vérification des données du formulaire de saisie
     * @param Représentation $uneRep représentation à vérifier
     * @param bool $creation : =true si formulaire de création d'une nouvelle représentation ; =false sinon
     */
    private function verifierDonneesRep(Representation $uneRep, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if (($creation && $uneRep->getId() == "") || $uneRep->getGroupe() == "" || $uneRep->getLieu() == "" || $uneRep->getDate() == "" ||
                $uneRep->getHeureDebut() == "" || $uneRep->getHeureFin() == "" ) {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $uneRep->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estAlphaNumerique($uneRep->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (RepresentationDAO::isAnExistingId($uneRep->getId())) {
                    GestionErreurs::ajouter("La représentation " . $uneRep->getId() . " existe déjà");
                }
            }
        }
        
    }
    
}