<?php

/**
 * Contrôleur de gestion des groupes
 * @author dloriot
 * @version 2020
 */

namespace controleur;

use controleur\GestionErreurs;
use modele\dao\GroupeDAO;
use modele\metier\Groupe;
use modele\dao\Bdd;
use vue\groupes\VueListeGroupes;
use vue\groupes\VueSaisieGroupe;
use vue\groupes\VueSupprimerGroupe;
use vue\groupes\VueDetailGroupe;

ini_set('display_errors', 'on');

class CtrlGroupes extends ControleurGenerique {

    /** controleur= groupes & action= defaut
     * Afficher la liste des groupes     */
    public function defaut() {
        $this->liste();
    }
    
     /** controleur= groupes & action= liste
     * Afficher la liste des groupes      */
    public function liste() {
        $laVue = new VueListeGroupes();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des groupes avec, pour chacun,
        //son nombre d'attributions de chambres actuel : 
        Bdd::connecter();
        $laVue->setLesGroupes($this->getTabGroupesAvecAttributions());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }  
    
    /** controleur= groupes & action=creer
     * Afficher le formulaire d'ajout d'un groupe     */
    public function creer() {
        $laVue = new VueSaisieGroupe();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouveau groupe");
        // En création, on affiche un formulaire vide
        /* @var Etablissement $unEtab */
        $unGroupe = new Groupe("", "", "", "", "", "", "");
        $laVue->setUnGroupe($unGroupe);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }

    /** controleur= groupes & action=validerCreer
     * ajouter d'un groupe dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        /* @var Groupe $unGroupe  : récupération du contenu du formulaire et instanciation d'un groupe */
        $unGroupe = new Groupe ($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['identiteResponsable'], $_REQUEST['adressePostale'], $_REQUEST['nombrePersonnes'], $_REQUEST['nomPays'], $_REQUEST['Hebergement']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesGroupe($unGroupe, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer le groupe
            GroupeDAO::insert($unGroupe);
            // revenir à la liste des groupes
            header("Location: index.php?controleur=groupes&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieGroupe();
            $this->vue = $laVue;
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Nouveau groupe");
            $laVue->setUnGroupe($unGroupe);
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - groupes");
            $this->vue->afficher();
        }
    }
    
    public function modifier() {
        $idGroupe = $_GET["id"];
        $laVue = new VueSaisieGroupe();
        $this->vue = $laVue;
        // Lire dans la BDD les données de le groupe à modifier
        Bdd::connecter();
        /* @var Groupe $leGroupe */
        $leGroupe = GroupeDAO::getOneById($idGroupe);
        $this->vue->setUnGroupe($leGroupe);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier le groupe : " . $leGroupe->getNom() . " (" . $leGroupe->getId() . ")");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }
    
    public function validerModifier() {
        Bdd::connecter();
        $unGroupe = new Groupe($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['identiteResponsable'], $_REQUEST['adressePostale'], $_REQUEST['nombrePersonnes'], $_REQUEST['nomPays'], $_REQUEST['Hebergement']);

        $this->verifierDonneesGroupe($unGroupe, false);
        if (GestionErreurs::nbErreurs() == 0) {
            GroupeDAO::update($unGroupe->getId(), $unGroupe);
            header("Location: index.php?controleur=groupes&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieGroupe();
            $this->vue = $laVue;
            $laVue->setUnGroupe($unGroupe);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier le groupe : " . $unGroupe->getNom() . " (" . $unGroupe->getId() . ")");
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - groupes");
            $this->vue->afficher();
        }
    }
    
    public function supprimer() {
        $idGroupe = $_GET["id"];
        $this->vue = new VueSupprimerGroupe();
        // Lire dans la BDD les données du groupe à supprimer
        Bdd::connecter();
        //$this->vue->setUnGroupe(GroupeDAO::getOneById($idGroupe));       
        $leGroupe = GroupeDAO::getOneById($idGroupe);
        $this->vue->setUnGroupe($leGroupe);
        
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }

    /** controleur= groupes & action= validerSupprimer
     * supprimer un groupe dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant du groupe à supprimer");
        } else {
            // suppression du groupe d'après son identifiant
            GroupeDAO::delete($_GET["id"]);
        }
        // retour à la liste des groupes
        header("Location: index.php?controleur=groupes&action=liste");
    }
    
    /** controleur= etablissements & action=detail & id=identifiant_établissement
     * Afficher un établissement d'après son identifiant     */
    public function detail() {
        $idGroupe = $_GET["id"];
        $this->vue = new VueDetailGroupe();
        // Lire dans la BDD les données de l'établissement à afficher
        Bdd::connecter();
        $this->vue->setUnGroupe(GroupeDAO::getOneById($idGroupe));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }
    
       private function verifierDonneesGroupe(Groupe $unGroupe, bool $creation) {
        if (($creation && $unGroupe->getId() == "") || $unGroupe->getNom() == "" || $unGroupe->getIdentite() == "" || $unGroupe->getAdresse() == "" ||
                $unGroupe->getNbPers() == "" || $unGroupe->getNomPays() == "" || $unGroupe->getHebergement() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        if ($creation && $unGroupe->getId() != "") {
            if (!estAlphaNumerique($unGroupe->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (GroupeDAO::isAnExistingId($unGroupe->getId())) {
                    GestionErreurs::ajouter("Le groupe: " . $unGroupe->getId() . " existe déjà");
                }
            }
        }
        if ($unGroupe->getNom() != "" && GroupeDAO::isAnExistingId($creation, $unGroupe->getId(), $unGroupe->getNom())) {
            GestionErreurs::ajouter("Le groupe: " . $unGroupe->getNom() . " existe déjà");
        }
    }

       public function getTabGroupesAvecAttributions(): Array {
        $lesGroupesAvecAttrib = Array();
        $lesGroupes = GroupeDAO::getAll();
        foreach ($lesGroupes as $unGroupe) {
            /* @var Groupes $unGroupe */
            $lesGroupesAvecAttrib[$unGroupe->getId()]['groupe'] = $unGroupe;
        }
        return $lesGroupesAvecAttrib;
    }
}