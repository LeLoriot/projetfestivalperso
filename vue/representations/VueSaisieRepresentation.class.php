<?php

namespace vue\representations;

use vue\VueGenerique;
use modele\metier\Representation;
use modele\dao\RepresentationDAO;
use modele\metier\Lieu;
use modele\dao\LieuDAO;
use modele\metier\Groupe;
use modele\dao\GroupeDAO;

use \PDO;
use \PDOException;
use controleur\GestionParametres;

 ini_set('display_errors', 'on');

 class VueSaisieRepresentation extends VueGenerique {

    private $uneRepresentation;

    /** @var string ="creer" ou = "modifier" en fonction de l'utilisation du formulaire */
    private $actionRecue;

    /** @var string ="validerCreer" ou = "validerModifier" en fonction de l'utilisation du formulaire */
    private $actionAEnvoyer;

    /** @var string à afficher en tête du tableau */
    private $message;
    
    /** @var Array liste de Lieu pour remplir le menu déroulant */
    private $listeLieu;
    
    /** @var Array liste de Groupe pour remplir le menu déroulant */
    private $listeGroupe;
    
    
    function getListeGroupe() {
        return $this->listeGroupe;
    }

    function getListeLieu() {
        return $this->listeLieu;
    }

    function setListeGroupe($listeGroupe) {
        $this->listeGroupe = $listeGroupe;
    }

    function setListeLieu($listeLieu) {
        $this->listeLieu = $listeLieu;
    }

       
    
    

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>

  


 <form method="POST" action="index.php?controleur=representations&action=<?= $this->actionAEnvoyer ?>">
            <br>
            <table width="85%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong><?= $this->message ?></strong></td>
                </tr>

                <?php
                // En cas de création, l'id est accessible à la saisie           
                if ($this->actionRecue == "creer") {
                    // On a le souci de ré-afficher l'id tel qu'il a été saisi
                    ?>
                    <tr class="ligneTabNonQuad">
                        <td> Id*: </td>
                        <td><input type="text" value="<?= $this->uneRepresentation->getId() ?>" name="id" size ="10" maxlength="8"></td>
                    </tr>
     
                    <tr class="ligneTabNonQuad">
                    <td>Lieu: </td>
                    <td> <select name="lieu">
                    <?php

                    foreach($this->listeLieu as $leLieu){
                        ?>         
                            <option value="<?=$leLieu->getId()?>" > <?=$leLieu->getNom()?> </option>
                    <?php
                    }
                    ?>
                        </select>
                    </td>
                    </tr>
                            
                    <tr class="ligneTabNonQuad">
                    <td>Groupe: </td>
                    <td> <select name="groupe">     
                            <?php

                    foreach($this->listeGroupe as $leGroupe){
                        ?>         
                            <option value="<?=$leGroupe->getId() ?>" ><?=$leGroupe->getNom()?></option>
                    <?php
                    }
                    ?>
                        </select>
                    </td>
                    </tr>
                    
                    <?php
                } else {
                    // Si on modifie l'enregistrement alors on execute le code suivant
                    ?>
                    <tr>
                        <td><input type="hidden" value="<?= $this->uneRepresentation->getId(); ?>" name="id"></td><td></td>
                    </tr>
                    <!-- Pour que le lieu afficher lors de la modification soit celui de l'enregistrement dans un premier temps-->
                    <tr class="ligneTabNonQuad">
                    <td>Lieu: </td>
                    
                    <td> <select name="lieu">
                            <?php
                            foreach($this->listeLieu as $leLieu){
                                $selected = "";
                                if ($leLieu->getId() == $this->uneRepresentation->getLieu()->getId()) {
                                
                                ?>
                            <option value="<?=$leLieu->getId()?>" selected="selected"><?=$leLieu->getNom()?></option>
                                <?php
                            }else{
                                ?>
                            <option value="<?=$leLieu->getId()?>"> <?=$leLieu->getNom()?></option>
                            <?php
                            }
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <!-- Pour que le groupe afficher lors de la modification soit celui de l'enregistrement -->
                <tr class="ligneTabNonQuad">
                    <td>Groupe: </td>
                    <td> <select name="groupe">
                            <?php
                            foreach($this->listeGroupe as $leGroupe){
                                $selected ="";
                                if ($leGroupe->getNom() == $this->uneRepresentation->getGroupe()->getNom()) {
                                ?>
                                <option value="<?=$leGroupe->getId()?>" selected="selected"><?=$leGroupe->getNom()?></option>
                                <?php
                                }else{
                                    ?>
                                    <option value="<?=$leGroupe->getId()?>"><?=$leGroupe->getNom()?></option>
                                <?php
                                }
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                    <?php
                }
                ?>
                    
                    
                    
                    
                <tr class="ligneTabNonQuad">
                    <td> Date*: </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getDate() ?>" name="date" 
                               size="50" maxlength="45"></td>
                </tr>
                
                <tr class="ligneTabNonQuad">
                    <td> Heure de début*: </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getHeureDebut() ?>" name="heureDebut" size="50" 
                               maxlength="45"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure de fin*: </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getHeureFin() ?>" name="heureFin" 
                               size="50" maxlength="45"></td>
                </tr>

                
            </table>

            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler">
                    </td>
                </tr>
            </table>
            <a href="index.php?controleur=representations&action=liste">Retour</a>
        </form>
        <?php
        include $this->getPied();
    }
    
        public function setUneRepresentation (Representation $uneRepresentation) {
        $this->uneRepresentation = $uneRepresentation;
    }
     public function setActionRecue(string $action) {
        $this->actionRecue = $action;
    }

    public function setActionAEnvoyer(string $action) {
        $this->actionAEnvoyer = $action;
    }

    public function setMessage(string $message) {
        $this->message = $message;
    }

    
}