<?php

namespace modele\dao;

use modele\metier\Representation;
use modele\metier\Lieu;
use modele\metier\Groupe;
use PDO;

/**
 * Description of RepresentationDAO
 * Classe métier :  Representation
 * @author dloiot
 * @version 2020
 */

class RepresentationDAO {

    /**
     * transforme un enregistrement issu de la table 'representation '
     * possiblement lié à plusieurs enregistrements de la table 'groupe' et 'lieu'
     * en un objet de type Representation
     * @param array $enreg enregistrement à traiter
     * @return Representation objet généré
     */
    public static function enregVersMetier(array $enreg) {
        // récupération de l'objet Categorie d'après son code
        //$idRepresentation = $enreg['ID'];
        $idGroupe = $enreg['IDGROUPE'];
        $idLieu = $enreg['IDLIEU'];
        //Récupération des objets de type groupe et lieu
        $leLieu = LieuDAO::getOneById($idLieu);
        $leGroupe = GroupeDAO::getOneById($idGroupe);
        //Instanciation de l'objet de type Representation
        $objetMetier = new Representation($enreg['ID'], $leGroupe,
                $leLieu, $enreg['DATE'], $enreg['HEUREDEBUT'], $enreg['HEUREFIN'] );
        
        return $objetMetier;        
    }

    /**
     * Complète une requête préparée
     * les paramètres de la requête associés aux valeurs des attributs d'un objet métier
     * @param Offre $objetMetier
     * @param PDOStatement $stmt
     */
    protected static function metierVersEnreg(Representation $objetMetier, \PDOStatement $stmt) {
        // On utilise bindValue plutôt que bindParam pour éviter des variables intermédiaires
        /* @var $unGroupe Groupe */
        $unGroupe = $objetMetier->getGroupe();
        /* @var $unLieu Lieu */
        $unLieu = $objetMetier->getLieu();
        $stmt->bindValue(':groupe', $unGroupe->getId());
        $stmt->bindValue(':lieu', $unLieu->getId());
        $stmt->bindValue(':id', $objetMetier->getId());
        $stmt->bindValue(':date', $objetMetier->getDate());
        $stmt->bindValue(':heureDebut', $objetMetier->getHeureDebut());
        $stmt->bindValue(':heureFin', $objetMetier->getHeureFin());
    }
    
    /**
     * Retourne la liste de toutes les Representation
     * @return array tableau d'objets de type Representation
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation ORDER BY date";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Tant qu'il y a des enregistrements dans la table
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                //ajoute un nouveau groupe au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    /**
     * Recherche une représentation selon la valeur de son identifiant
     * @param string $id
     * @return Representation représentation trouvé ; null sinon
     */
     public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Representation WHERE id = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }

    
    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Representation $objet objet métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Representation $objet) {
        $requete = "INSERT INTO Representation VALUES (:id, :groupe, :lieu, :date, :heureDebut, :heureFin)";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Mettre à jour enregistrement dans la table à partir de l'état d'un objet métier
     * @param string identifiant de l'enregistrement à mettre à jour
     * @param Representation $objet objet métier à mettre à jour
     * @return boolean =FALSE si l'opérationn échoue
     */
    public static function update($id, Representation $objet) {
        $ok = false;
        $requete = "UPDATE  Representation SET IDGROUPE= :groupe, IDLIEU= :lieu,
           DATE= :date, HEUREDEBUT= :heureDebut, HEUREFIN= :heureFin
           WHERE ID= :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }
    

     /**
     * Détruire un enregistrement de la table REPRESENTATION d'après son identifiant
     * @param string identifiant de l'enregistrement à détruire
     * @return boolean =TRUE si l'enregistrement est détruit, =FALSE si l'opération échoue
     */
    public static function delete($id) {
        $ok = false;
        $requete = "DELETE FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }
    
    /**
     * Permet de vérifier s'il existe ou non une représentation ayant déjà le même identifiant dans la BD
     * @param string $id identifiant de la représentation à tester
     * @return boolean =true si l'id existe déjà, =false sinon
     */
    public static function isAnExistingId($id) {
        $requete = "SELECT COUNT(*) FROM Representation WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }
    
}
